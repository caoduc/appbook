package com.example.bansach.Book;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.bansach.Adapter.BookAdapter;
import com.example.bansach.Cart.CartActivity;
import com.example.bansach.Firebase.BookFirebase;
import com.example.bansach.Model.BookModel;
import com.example.bansach.R;

import java.util.ArrayList;

public class BookDetailActivity extends AppCompatActivity {
    public static ArrayList<BookModel> list;
    public static BookAdapter adapter;
    BookFirebase bookFirebase;
    SwipeMenuListView lvBook;
    Button btnThemVaoGio,btnBack;
    ImageButton btnCart;
    TextView tvMasach;
    TextView tvTenSach,tvTacGia,tvTheLoai,tvNhaXB,tvMota,tvGia;
    ImageView imgHinhAnh;
    int tgia,tsoluong;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_book_detail);getSupportActionBar().hide();
        btnCart=findViewById(R.id.btnCart);
        btnThemVaoGio=findViewById(R.id.btnThemVaoGio);
        imgHinhAnh=findViewById(R.id.imgHinhAnh);
        btnBack=findViewById(R.id.btnBack);
        tvTenSach=findViewById(R.id.tvTenSach);
        tvMasach=findViewById(R.id.tvMaSach);
        tvTacGia=findViewById(R.id.tvTacGia);
        tvTheLoai=findViewById(R.id.tvTheloai);
        tvNhaXB=findViewById(R.id.tvNhaXB);
        tvMota=findViewById(R.id.tvMota);
        tvGia=findViewById(R.id.tvGia);
        bookFirebase=new BookFirebase(BookDetailActivity.this);
        Intent in = getIntent();
        Bundle b = in.getExtras();
        if (b != null) {
            tvMasach.setText(b.getString("MASACH"));
            tvTenSach.setText(b.getString("TENSACH"));
            tvTacGia.setText(b.getString("TACGIA"));
            tvNhaXB.setText(b.getString("NXB"));
            tvTheLoai.setText(b.getString("THELOAI"));
            tvGia.setText(b.getString("GIABIA"));
        }
        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(BookDetailActivity.this, CartActivity.class);
                startActivity(in);
            }
        });


    }
}
