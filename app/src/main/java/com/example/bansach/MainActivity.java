package com.example.bansach;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.bansach.BestSeller.BestSellerActivity;
import com.example.bansach.Firebase.UserFirebase;
import com.example.bansach.Bill.ListBillActivity;
import com.example.bansach.Account.ChangePasswordActivity;
import com.example.bansach.Account.LogInActivity;
import com.example.bansach.Fragment.BookHightlightActivity;
import com.example.bansach.Model.UserModel;
import com.example.bansach.User.ListUserActivity;
import com.example.bansach.User.MyUserActivity;
import com.example.bansach.Book.ListBookActivity;
import com.example.bansach.Category.ListCategoryActivity;
import com.example.bansach.Statistics.StatisticsActivity;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    CardView user, category, book, bill, highlight, statistics, changepassword, logout,banchay;
    static TextView hello;
    public static String tenTk = "";
    UserFirebase userFireBase;
    static ArrayList<UserModel> userModelArrayList;
    public static Boolean checkAdmin;
    LoadingDialog load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        Intent i = getIntent();
        tenTk = i.getStringExtra("user");
        userFireBase = new UserFirebase(this);
        userModelArrayList = userFireBase.getAllNone();
        init();
        load = new LoadingDialog(this);

    }

    public void checkAdmin() {
        if (tenTk.matches("caotanduc@gmail.com")) {
            hello.setText("Admin");

            checkAdmin = true;
//            banchay.setVisibility(View.VISIBLE);
            highlight.setVisibility(View.VISIBLE);
            statistics.setVisibility(View.VISIBLE);
            changepassword.setVisibility(View.GONE);

            ((android.widget.TextView) findViewById(R.id.textHello)).setText("ADMIN - ");
            ((android.widget.TextView) findViewById(R.id.tenUser)).setTextColor(Color.RED);
            ((android.widget.TextView) findViewById(R.id.titleUser)).setText("Danh sách người dùng");
            ((android.widget.TextView) findViewById(R.id.titleHighlight)).setText("Top sách bán chạy");
            ((android.widget.ImageView) findViewById(R.id.imgHighlight)).setImageResource(R.drawable.bestsell);

        } else {
            checkAdmin = false;
            category.setVisibility(View.GONE);
//            banchay.setVisibility(View.GONE);
            highlight.setVisibility(View.VISIBLE);
            statistics.setVisibility(View.GONE);
            ((android.widget.ImageView) findViewById(R.id.imgHighlight)).setImageResource(R.drawable.outstanding);
            ((android.widget.TextView) findViewById(R.id.titleUser)).setText("Thông tin người dùng");
            ((android.widget.TextView) findViewById(R.id.titleHighlight)).setText("Bộ sách nổi bật");
        }
        hello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, MyUserActivity.class);
                startActivity(in);
            }
        });
    }

    public static void setTenTk() {
        for (int i = 0; i < userModelArrayList.size(); i++) {
            String ten = userModelArrayList.get(i).getUserName();
            if (ten.matches(tenTk)) {
                if (userModelArrayList.get(i).getHoTen().isEmpty()) {
                    hello.setText(tenTk);
                } else {
                    hello.setText(userModelArrayList.get(i).getHoTen());
                }
                break;
            }
        }
    }

    private void init() {
        user = findViewById(R.id.menuUser);
        category = findViewById(R.id.menuCategory);
        book = findViewById(R.id.menuBook);
        bill = findViewById(R.id.menuBill);
//        banchay = findViewById(R.id.mnbanchay);
        highlight=findViewById(R.id.menuHighlight);
        statistics = findViewById(R.id.menuStatistics);
        changepassword = findViewById(R.id.menuChangePassword);
        logout = findViewById(R.id.menuLogOut);
        hello = findViewById(R.id.tenUser);

        checkAdmin();

        user.setOnClickListener(this);
        category.setOnClickListener(this);
        book.setOnClickListener(this);
        bill.setOnClickListener(this);
//        banchay.setOnClickListener(this);
        highlight.setOnClickListener(this);
        statistics.setOnClickListener(this);
        changepassword.setOnClickListener(this);
        logout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            Activity x = null;
            switch (v.getId()) {
                case R.id.menuUser:
                    if (MainActivity.checkAdmin == false) {
                        //load.startLoad(2);
                        x = new MyUserActivity();
                        break;
                    } else {
                        // load.startLoad(2);
                        x = new ListUserActivity();
                        // x = new MyUserActivity();
                        break;
                    }
                case R.id.menuCategory:

                    x = new ListCategoryActivity();
                    break;
                case R.id.menuBook:
                    x = new ListBookActivity();
                    break;
                case R.id.menuBill:
                    x = new ListBillActivity();
                    break;
//                case R.id.mnbanchay:
//                    x = new BestSellerActivity();
//                    break;
                case R.id.menuHighlight:
                    if(MainActivity.checkAdmin==true){
                        x=new BestSellerActivity();
                    }
                    else
                        x = new BookHightlightActivity();
                    break;
                case R.id.menuStatistics:
                    x = new StatisticsActivity();
                    break;
                case R.id.menuChangePassword:
                    x = new ChangePasswordActivity();
                    break;
                case R.id.menuLogOut:
                    x = new LogInActivity();
                    break;
            }
            Intent i = new Intent(this, x.getClass());
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
