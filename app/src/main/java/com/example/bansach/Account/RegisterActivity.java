package com.example.bansach.Account;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.bansach.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private String Name, Phone, Email, Password, RePassword, saveCurrentDate, saveCurrentTime;
    private Button Register;
    private ImageView InputUserImage;
    private EditText InputUserName, InputUserPhone, InputUserEmail, InputPassword, InputRePassword;
    private static final int GalleryPick = 1;
    private Uri ImageUri;
    private String userRandomKey, downloadImageUrl;
    private StorageReference UserImagesRef;
    private DatabaseReference UsersRef;
    private ProgressDialog loadingBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        getSupportActionBar().hide();
//        CategoryName = getIntent().getExtras().get("category").toString();
        UserImagesRef = FirebaseStorage.getInstance().getReference().child("User Images");
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        Register = (Button) findViewById(R.id.btnRegisterUS);
        InputUserImage = (ImageView) findViewById(R.id.selectPhotoAvatar);
        InputUserName = (EditText) findViewById(R.id.edNameUS);
        InputUserPhone = (EditText) findViewById(R.id.edPhoneUS);
        InputUserEmail = (EditText) findViewById(R.id.edEmailUS);
        InputPassword=findViewById(R.id.edPasswordUS);
        InputRePassword=findViewById(R.id.edRePasswordUS);
        loadingBar = new ProgressDialog(this);


        InputUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                OpenGallery();
            }
        });


        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ValidateUserData();
            }
        });
    }
    private void OpenGallery()
    {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GalleryPick);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==GalleryPick  &&  resultCode==RESULT_OK  &&  data!=null)
        {
            ImageUri = data.getData();
            InputUserImage.setImageURI(ImageUri);
        }
    }


    private void ValidateUserData()
    {
        Phone = InputUserPhone.getText().toString();
        Email = InputUserEmail.getText().toString();
        Password=InputPassword.getText().toString();
        RePassword=InputRePassword.getText().toString();
        Name = InputUserName.getText().toString();


        if (ImageUri == null) {
            Toast.makeText(this, "Chưa chọn hình đại diện", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(Phone)) {
            InputUserPhone.setError("Số điện thoại không được để trống");
            Toast.makeText(this, "Vui lòng điền số điện thoại", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(Email)) {
            InputUserEmail.setError("Địa chỉ email không được để trống");
            Toast.makeText(this, "Vui lòng điền email", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(Name)) {
            InputUserName.setError("Họ tên không được để trống");
            Toast.makeText(this, "Vui lòng nhập họ tên", Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(Password)){
            InputPassword.setError("Mật khẩu không được để trống");
            Toast.makeText(this, "Vui lòng nhập mật khẩu", Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(RePassword)){
            InputRePassword.setError("Mật khẩu nhập lại không được để trống");
            Toast.makeText(this, "Vui lòng nhập lại mật khẩu", Toast.LENGTH_SHORT).show();
        }else if (InputPassword.length() < 6) {
            InputPassword.setError("Mật khẩu tối thiểu 6 ký tự");
            Toast.makeText(getApplicationContext(), "Mật khẩu phải có ít nhất 6 ký tự",
                    Toast.LENGTH_SHORT).show();
        }else if (InputUserPhone.getText().toString().length() < 10 || InputUserPhone.getText().toString().length() > 10) {
            InputUserPhone.setError("Số điện thoại không đúng định dạng");
            Toast.makeText(getApplicationContext(), "Vui lòng nhập đúng số điện thoại!", Toast.LENGTH_SHORT).show();
        } else{
            StoreUserInformation();
        }
    }

    private void StoreUserInformation()
    {
        loadingBar.setTitle("Đăng ký tài khoản");
        loadingBar.setMessage("Vui lòng đợi đến khi tài khoản được tạo thành công...");
        loadingBar.setCanceledOnTouchOutside(false);
        loadingBar.show();

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss a");
        saveCurrentTime = currentTime.format(calendar.getTime());

        userRandomKey = saveCurrentDate + saveCurrentTime;


        final StorageReference filePath = UserImagesRef.child(ImageUri.getLastPathSegment() + userRandomKey + ".jpg");

        final UploadTask uploadTask = filePath.putFile(ImageUri);


        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                String message = e.toString();
                Toast.makeText(RegisterActivity.this, "Error: " + message, Toast.LENGTH_SHORT).show();
                loadingBar.dismiss();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
            {
                Toast.makeText(RegisterActivity.this, "Cập nhật hình đại diện thành công", Toast.LENGTH_SHORT).show();

                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
                    {
                        if (!task.isSuccessful())
                        {
                            throw task.getException();
                        }

                        downloadImageUrl = filePath.getDownloadUrl().toString();
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task)
                    {
                        if (task.isSuccessful())
                        {
                            downloadImageUrl = task.getResult().toString();

                            Toast.makeText(RegisterActivity.this, "___", Toast.LENGTH_SHORT).show();

                            SaveUserInfoToDatabase();
                        }
                    }
                });
            }
        });
    }



    private void SaveUserInfoToDatabase()
    {
        HashMap<String, Object> userMap = new HashMap<>();
        userMap.put("uid", userRandomKey);
        userMap.put("uname", Name);
        userMap.put("uphone", Phone);
        userMap.put("uemail", Email);
        userMap.put("uimage", downloadImageUrl);
        userMap.put("upass", Password);
        userMap.put("urepass", RePassword);
        userMap.put("date", saveCurrentDate);
        userMap.put("time", saveCurrentTime);

        UsersRef.child(userRandomKey).updateChildren(userMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful())
                        {
                            Intent intent = new Intent(RegisterActivity.this, LogInActivity.class);
                            startActivity(intent);

                            loadingBar.dismiss();
                            Toast.makeText(RegisterActivity.this, "Đăng ký tài khoản thành công", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            loadingBar.dismiss();
                            String message = task.getException().toString();
                            Toast.makeText(RegisterActivity.this, "Error: " + message, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
