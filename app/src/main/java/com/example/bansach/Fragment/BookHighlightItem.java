package com.example.bansach.Fragment;
import com.example.bansach.R;

import java.io.Serializable;
import java.util.List;

public class BookHighlightItem implements Serializable {
    private int image;
    private String name,detail;
    List<BookHighlightItem> mdata;

    public BookHighlightItem() {
    }

    public BookHighlightItem(int image, String name, String detail) {
        this.image = image;
        this.name = name;
        this.detail = detail;
    }

    public BookHighlightItem(int image, String name) {
        this.image = image;
        this.name = name;
    }
//    public BookHighlightItem(int image) {
//        this.image = image;
//    }



    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
