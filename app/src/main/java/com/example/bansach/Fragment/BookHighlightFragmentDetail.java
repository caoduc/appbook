package com.example.bansach.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.bansach.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class BookHighlightFragmentDetail extends BottomSheetDialogFragment {
    private TextView bookName,bookDetail;
    private ImageView bookImg;
    public BookHighlightFragmentDetail() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookhighlight_detail, container, false);
        bookName =view.findViewById(R.id.txtBookName);
        bookDetail =view.findViewById(R.id.txtBookDetail);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bookImg=view.findViewById(R.id.imgBook);
        bookName=view.findViewById(R.id.txtBookName);
        bookDetail=view.findViewById(R.id.txtBookDetail);
        Bundle bundle = getArguments();
        BookHighlightItem item = (BookHighlightItem) bundle.getSerializable("object");
        loadBookData(item);
    }

    void loadBookData(BookHighlightItem item){
        Glide.with(getContext()).load(item.getImage()).into(bookImg);
    }
}