package com.example.bansach.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.bansach.R;

import java.util.ArrayList;
import java.util.List;


public class BookHighlightFragment extends Fragment implements BookHighlightCallback {

    List<BookHighlightItem> mdata;
    RecyclerView rv_bookhighlight;
    BookHightlightAdapter bookHightlightAdapter;


    public BookHighlightFragment() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bookhighlight, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv_bookhighlight=view.findViewById(R.id.rvBookHighlight);
        mdata=new ArrayList<>();
        mdata.add(new BookHighlightItem(R.drawable.b1,"1skskl","aaaaaaaa"));
        mdata.add(new BookHighlightItem(R.drawable.b2,"2ksjakjg","aaaaaaaa"));
        mdata.add(new BookHighlightItem(R.drawable.b3s,"3agkgjkajgka","aaaaaaaa"));
        mdata.add(new BookHighlightItem(R.drawable.b4,"4k4dagjkajlg"));
        mdata.add(new BookHighlightItem(R.drawable.b5,""));
        mdata.add(new BookHighlightItem(R.drawable.b6s,""));
        mdata.add(new BookHighlightItem(R.drawable.b7,""));
        mdata.add(new BookHighlightItem(R.drawable.b8,""));

        bookHightlightAdapter = new BookHightlightAdapter(mdata,this);
         rv_bookhighlight.setLayoutManager(new GridLayoutManager(getActivity(),3));
         rv_bookhighlight.setAdapter(bookHightlightAdapter);
    }

    @Override
    public void onBookHighlightItemClick(int pos) {
        BookHighlightFragmentDetail bookHLFragment =
                new BookHighlightFragmentDetail();
        Bundle bundle = new Bundle();
        bundle.putSerializable("object",mdata.get(pos));
        bundle.putSerializable("name",mdata.get(pos));
        bundle.putSerializable("detail", mdata.get(pos));
        bookHLFragment.setArguments(bundle);
        bookHLFragment.show(getActivity().getSupportFragmentManager(),"tagname");

    }
}