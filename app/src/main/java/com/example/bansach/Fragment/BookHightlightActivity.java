package com.example.bansach.Fragment;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bansach.R;

public class BookHightlightActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_book_hightlight); getSupportActionBar().hide();
        setBookHighlightfragment();


    }
    void setBookHighlightfragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new BookHighlightFragment()).commit();
    }
}