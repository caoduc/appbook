package com.example.bansach.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bansach.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class BookHightlightAdapter extends RecyclerView.Adapter<BookHightlightAdapter.BookHighlightViewHolder> {
    List<BookHighlightItem> mdata;

    BookHighlightCallback listener;

    public BookHightlightAdapter(List<BookHighlightItem> mdata,BookHighlightCallback listener){
        this.mdata=mdata;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BookHighlightViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bookhighlight,parent,false);
        return new BookHighlightViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull BookHighlightViewHolder holder, int position) {
        Glide.with(holder.itemView.getContext()).load
                (mdata.get(position).getImage()).into(holder.imgport);
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    public class BookHighlightViewHolder extends RecyclerView.ViewHolder{
        ImageView imgport;


        public BookHighlightViewHolder(View itemView){
            super(itemView);
            imgport = itemView.findViewById(R.id.item_bookHighlight_img);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onBookHighlightItemClick(getAdapterPosition());
                }
            });
        }
    }
}
