package com.example.bansach.Fragment;

public interface BookHighlightCallback {
    void onBookHighlightItemClick(int pos);
}
