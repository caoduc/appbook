package com.example.bansach.User;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bansach.R;
import com.example.bansach.Firebase.UserFirebase;
import com.example.bansach.Model.UserModel;

public class EditUserActivity extends AppCompatActivity {
    EditText edFullName, edPhone, edEmail;
    String username, fullname, phone, pass;

    UserFirebase userFirebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_edit_user);getSupportActionBar().hide();
        edFullName = (EditText) findViewById(R.id.edFullName);
        edPhone = (EditText) findViewById(R.id.edPhone);
        edEmail = findViewById(R.id.edEmail);
        userFirebase = new UserFirebase(EditUserActivity.this);
        ImageView avatar = findViewById(R.id.avatarUserEdit);
        avatar.setImageResource(R.drawable.pin);
        init();

    }

    public void init() {
        Intent in = getIntent();
        Bundle b = in.getExtras();
        fullname = b.getString("FULLNAME");
        phone = b.getString("PHONE");
        username = b.getString("USERNAME");
        pass = b.getString("PASSWORD");
        edFullName.setText(fullname);
        edPhone.setText(phone);
        edEmail.setText(username);
        edEmail.setKeyListener(null);
        edEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Không được phép thay đổi", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void updateUser(View view) {
        String hoTen, sdt, username;
        hoTen = edFullName.getText().toString();
        sdt = edPhone.getText().toString();
        username = edEmail.getText().toString();

        if (hoTen.isEmpty() || sdt.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Các trường không được để trống!", Toast.LENGTH_SHORT).show();
        } else if (sdt.length() < 10 || sdt.length() > 12) {
            Toast.makeText(getApplicationContext(), "Vui lòng nhập đúng số điện thoại!", Toast.LENGTH_SHORT).show();
        } else {
            UserModel userModel = new UserModel(username, pass, sdt, hoTen);
            userFirebase.update(userModel);
            Toast.makeText(getApplicationContext(), "Sửa thành công", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void Huy(View view) {
        finish();
    }
}
